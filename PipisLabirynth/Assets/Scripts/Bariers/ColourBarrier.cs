using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColourBarrier : MonoBehaviour  //umieszczona w barierach wszelkiego koloru, kooperuje scisle z klasa okreslajaca konkretny kolor bariery
{                                           //za pomoca parametru ColourName,  obsluguje otwieranie i zamykanie barier 
    private bool isGateOpen = false;
    private Animator animator;
    private string ColourName; 

    void Start()
    {
        animator = gameObject.GetComponent<Animator>();
    }
    public void SetName(string colourName)
    {
        this.ColourName = colourName;
    }
    public void TurnBariers(bool isBarierClosed)
    {
        if (isBarierClosed)
            OpenGatesAnimation();
        else
            CloseGatesAnimation();
    }
    public void CloseBariers(string colourName)
    {
        if (colourName != ColourName)
        {
            CloseGateOrReturnIfAlreadyClosed();
        }
        else return;
    }
    private void CloseGateOrReturnIfAlreadyClosed()
    {
        if (isGateOpen)
            CloseGatesAnimation();
        else return;
    }
    private void CloseGatesAnimation()
    {        
        animator.SetTrigger("GateShow");
        isGateOpen = false;
    }
    private void OpenGatesAnimation()
    {
        animator.SetTrigger("GateHide");
        isGateOpen = true;
    }    
}
