using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PurpleBarier : MonoBehaviour, IColourBarier
{
    private ColourBarrier colourBarrier;    
    private string ColourName = "Purple";

    void Start()
    {
        colourBarrier = gameObject.GetComponent<ColourBarrier>();
        colourBarrier.SetName(ColourName);
    }
    public void TurnBariers(bool isBarierClosed)
    {
        colourBarrier.TurnBariers(isBarierClosed);
    }
    
    public void CloseBariers(string name)
    {
        colourBarrier.CloseBariers(name);        
    }    
}
