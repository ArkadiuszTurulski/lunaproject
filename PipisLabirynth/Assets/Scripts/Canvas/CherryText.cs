using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CherryText : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI cherryText;

    public void TextNumberOfLeftCherries(int cherrysNum)
    {
        cherryText.text = cherrysNum.ToString();
    }
}
