using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fade : MonoBehaviour   //obsluguje animacje zacieminajaca obraz przy przechodzeniu do nastepnego levelu
{
    private Animator animator;

    void Start()
    {
        animator = gameObject.GetComponent<Animator>();
    }

    public void FadeStart()
    {
        animator.SetTrigger("FadeOn");
    }
}
