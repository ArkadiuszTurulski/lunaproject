using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Door : MonoBehaviour
{
    //Drzwi s� drog� do nast�pnego levelu
    //Nale�y zebra� wszystkie wi�nie aby przy kolizji playera z drzwiami uaktywni� si� mechanizm �aduj�cy kolejny level
    //Je�li nie wszystkie wi�nie s� zebrane, na ekranie pojawia si� informacja o tym, za� mechanizm si� nie uaktywnia.
    [SerializeField] float transitionTime = 1f;    
    private Fade fadePanel;
    

    void Start()
    {
        fadePanel = FindObjectOfType<Fade>();
    }
    public void Restart()
    {
        StartCoroutine(LoadScene(GetCurrentSceneIndex()));
    }
    public void LoadNextLevel()
    {
            if (isThisTheLastLevel()) 
                SceneManager.LoadScene(0);
            else 
                StartCoroutine(LoadScene(GetCurrentSceneIndex() + 1));
    }

    private bool isThisTheLastLevel()
    {
        if (GetCurrentSceneIndex() < 6) return false;
        else return true;
    }    
    private int GetCurrentSceneIndex() 
    { 
        var currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        return currentSceneIndex;
    }
    IEnumerator LoadScene(int sceneIndex)
    {
        fadePanel.FadeStart();
        yield return new WaitForSeconds(transitionTime);
        SceneManager.LoadScene(sceneIndex);
    }
}
