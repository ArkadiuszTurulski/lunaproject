using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{

    [SerializeField] float movingSpeed;
    [SerializeField] GameObject child;
    public bool ifMovingDirectionHorizontal; //ustalamy r�cznie czy obiekt ma si� porusza� poziomo czy pionowo
    private Vector3 movingDirection;
    private float ifMultiplayedByNegativeReverseDirection = 1f; //zmienna przyjmuje warto�� z przeciwnym znakiem przy ka�dym zetkni�ciu ze �cian�
    private Vector2 objectSize; 

    void Start()
    {   
        objectSize = child.transform.localScale;
    }
    
    void Update()
    {
        EnemyMoving();
    }

    private void EnemyMoving()
    {
        CheckIfVerticalOrHorizontalDirection();
        transform.Translate(movingDirection * movingSpeed * Time.deltaTime);
    }
    private void CheckIfVerticalOrHorizontalDirection()
    {
        if (ifMovingDirectionHorizontal)
            movingDirection = new Vector3(ifMultiplayedByNegativeReverseDirection, 0f, 0f);
        else
            movingDirection = new Vector3(0f, ifMultiplayedByNegativeReverseDirection, 0f);
    }


    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<SnapToGrid>())
            FlipMovingDirection();
    }
    private void FlipMovingDirection()
    {
        ifMultiplayedByNegativeReverseDirection *= -1f;
        FlipEnemyObjectSizetoOpposite();
    }
    private void FlipEnemyObjectSizetoOpposite()
    {        
        objectSize.x *= -1f;
        child.transform.localScale = objectSize;
    }
    
}
