using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class NewJoystick : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler
{
    [SerializeField] Image backgroundImage;
    [SerializeField] Image joystickImage;
    Vector2 touchPosition;
    public Vector3 directionVector { set; get; }
    private bool stopJoystick = false;

    private void Start()
    {
        directionVector = Vector3.zero;        
    }

    public void StartJoystick()
    {
        stopJoystick = false;
    }
    public void StopJoystick()
    {
        stopJoystick = true;
    }
    public Vector2 GetDirectionVector()//Konwertowany tutaj z Vector3 na Vector2!
    {
        if (!stopJoystick) return new Vector2(directionVector.x, directionVector.z); 
        else return Vector2.zero;
    }


    public virtual void OnPointerDown(PointerEventData ped)
    {
        OnDrag(ped);
    }
    public virtual void OnPointerUp(PointerEventData ped)
    {
        directionVector = Vector3.zero;
        SetJoystickImageToZeroPosition();
    }
    public virtual void OnDrag(PointerEventData ped)
    {
        touchPosition = Vector2.zero;
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle
        (backgroundImage.rectTransform, ped.position, ped.pressEventCamera, out touchPosition))
        {
            FindTouchPositionOnBackgroundImage();
            ConvertTouchPositionToDirectionVector();
            NormalizeDirectionVectorIfLongerThanOne();
            PreventPlayerFromQuickSwitchingDirectionIfVelocityIsToSmall();
            ShiftJoystickImageAccordingToTheDirectionVector();
        }
    }
    private void FindTouchPositionOnBackgroundImage()
    {
        touchPosition.x = (touchPosition.x / backgroundImage.rectTransform.sizeDelta.x);
        touchPosition.y = (touchPosition.y / backgroundImage.rectTransform.sizeDelta.y);
    }
    private void ConvertTouchPositionToDirectionVector()
    {
        directionVector = new Vector3(touchPosition.x, 0, touchPosition.y);
    }
    private void NormalizeDirectionVectorIfLongerThanOne()
    {
        directionVector = (directionVector.magnitude > 1) ? directionVector.normalized : directionVector;
    }
    private void PreventPlayerFromQuickSwitchingDirectionIfVelocityIsToSmall()
    {
        directionVector = (directionVector.magnitude < 0.1f) ? directionVector = Vector3.zero : directionVector;
    }
    private void ShiftJoystickImageAccordingToTheDirectionVector()
    {
        joystickImage.rectTransform.anchoredPosition = new Vector3(directionVector.x * (backgroundImage.rectTransform.sizeDelta.x / 3),
            directionVector.z * (backgroundImage.rectTransform.sizeDelta.y / 3));
    }
    private void SetJoystickImageToZeroPosition()
    {
        joystickImage.rectTransform.anchoredPosition = Vector3.zero;
    }



}