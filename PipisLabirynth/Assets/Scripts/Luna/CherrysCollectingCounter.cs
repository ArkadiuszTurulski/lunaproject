using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CherrysCollectingCounter : MonoBehaviour
{    
    private int cherrysLeftNumber;
    private CherryText cherryCounterText; //Licznik wisni do zebrania, widoczny w lewym gornym rogu ekranu

    void Start()
    {
        cherryCounterText = FindObjectOfType<CherryText>();
        ShowHowManyCherriesToCollect();
        IssueService();
    }
    
    public void CollectTheCherry()
    {
        ActivateCherryCollectingAudioClip();
        cherrysLeftNumber--;
        ShowOnTheDisplayNumberOfCherries();
    }

    private void ShowHowManyCherriesToCollect()
    {
        CountEveryCherry();
        ShowOnTheDisplayNumberOfCherries();
    }
    private void CountEveryCherry() 
    {
        cherrysLeftNumber = FindObjectsOfType<Cherry>().Length;
    }
    private void ShowOnTheDisplayNumberOfCherries()
    {        
        cherryCounterText.TextNumberOfLeftCherries(cherrysLeftNumber);
    }
    private void ActivateCherryCollectingAudioClip()
    {
        FindObjectOfType<GameSounds>().CollectingCherrySound();
    }
    private void IssueService()
    {
        if (cherryCounterText == null) Debug.Log("CherrysCollectingCounter.Start. Can't find cherryCounterText.");
        else return;
    }
}
