using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionWihBlueScreen : MonoBehaviour
{
    private BlueScreen[] listOfBlueScreens;
    private BlueBarier[] listOfBlueBariers;
    private GateCloser gateCloser;

    void Start()
    {
        listOfBlueScreens = FindObjectsOfType<BlueScreen>();
        listOfBlueBariers = FindObjectsOfType<BlueBarier>();
        gateCloser = gameObject.GetComponent<GateCloser>();
        IssueService();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<BlueScreen>())
        {
            var isBarierClosed = other.gameObject.GetComponent<BlueScreen>().IsScreenTurnOn();
            TurnBlueScreens();            
            TurnBlueGates(isBarierClosed);
            gateCloser.CloseTheGatesExceptBlue();
        }
    }
    private void TurnBlueScreens()
    {
        foreach (BlueScreen item in listOfBlueScreens)
        {
            item.TurnScreens();
        }        
    }
    private void TurnBlueGates(bool isBarierClosed)
    {
        foreach (BlueBarier item in listOfBlueBariers)
        {
            item.TurnBariers(isBarierClosed);
        }        
    }
    private void IssueService()
    {
        if (listOfBlueScreens == null) Debug.Log("CollisionWihBlueScreen.Start. Can't find listOfBlueScreens.");
        if (listOfBlueBariers == null) Debug.Log("CollisionWihBlueScreen.Start. Can't find listOfBlueBariers.");
        if (gateCloser == null) Debug.Log("CollisionWihBlueScreen.Start. Can't find gateCloser.");
        else return;
    }
}
