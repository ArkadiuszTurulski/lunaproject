using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionWithGreenScreen : MonoBehaviour
{
    private GreenScreen[] listOfGreenScreens;
    private GreenBarier[] listOfGreenBariers;
    private GateCloser gateCloser;

    void Start()
    {
        listOfGreenScreens = FindObjectsOfType<GreenScreen>();
        listOfGreenBariers = FindObjectsOfType<GreenBarier>();
        gateCloser = gameObject.GetComponent<GateCloser>();
        IssueService();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<GreenScreen>())
        {
            var isBarierClosed = other.gameObject.GetComponent<GreenScreen>().IsScreenTurnOn();
            TurnGreenScreens();
            TurnGreenGates(isBarierClosed);
            gateCloser.CloseTheGatesExceptGreen();
        }
    }
    private void TurnGreenScreens()
    {
        foreach (GreenScreen item in listOfGreenScreens)
        {
            item.TurnScreens();
        }        
    }
    private void TurnGreenGates(bool isBarierClosed)
    {        
        foreach (GreenBarier item in listOfGreenBariers)
        {
            item.TurnBariers(isBarierClosed);
        }        
    }
    private void IssueService()
    {
        if (listOfGreenScreens == null) Debug.Log("CollisionWihGreenScreen.Start. Can't find listOfGreenScreens.");
        if (listOfGreenBariers == null) Debug.Log("CollisionWihGreenScreen.Start. Can't find listOfGreenBariers.");
        if (gateCloser == null) Debug.Log("CollisionWihGreenScreen.Start. Can't find gateCloser.");
        else return;
    }
}
