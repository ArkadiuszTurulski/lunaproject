using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionWithPinkScreen : MonoBehaviour
{
    private PinkScreen[] listOfPinkScreens;
    private PinkBarier[] listOfPinkBariers;
    private GateCloser gateCloser;

    void Start()
    {
        listOfPinkScreens = FindObjectsOfType<PinkScreen>();
        listOfPinkBariers = FindObjectsOfType<PinkBarier>();
        gateCloser = gameObject.GetComponent<GateCloser>();
        IssueService();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<PinkScreen>())
        {
            var isBarierClosed = other.gameObject.GetComponent<PinkScreen>().IsScreenTurnOn();
            TurnPinkScreens();
            TurnPinkGates(isBarierClosed);
            gateCloser.CloseTheGatesExceptPink();
        }
    }
    private void TurnPinkScreens()
    {
        foreach (PinkScreen item in listOfPinkScreens)
        {
            item.TurnScreens();
        }        
    }
    private void TurnPinkGates(bool isBarierClosed)
    {        
        foreach (PinkBarier item in listOfPinkBariers)
        {
            item.TurnBariers(isBarierClosed);
        }        
    }
    private void IssueService()
    {
        if (listOfPinkScreens == null) Debug.Log("CollisionWihPinkScreen.Start. Can't find listOfPinkScreens.");
        if (listOfPinkBariers == null) Debug.Log("CollisionWihPinkScreen.Start. Can't find listOfPinkBariers.");
        if (gateCloser == null) Debug.Log("CollisionWihPinkScreen.Start. Can't find gateCloser.");
        else return;
    }
}
