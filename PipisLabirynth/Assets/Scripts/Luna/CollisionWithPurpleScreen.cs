using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionWithPurpleScreen : MonoBehaviour
{
    private PurpleScreen[] listOfPurpleScreens;
    private PurpleBarier[] listOfPurpleBariers;
    private GateCloser gateCloser;

    void Start()
    {
        listOfPurpleScreens = FindObjectsOfType<PurpleScreen>();
        listOfPurpleBariers = FindObjectsOfType<PurpleBarier>();
        gateCloser = gameObject.GetComponent<GateCloser>();
        IssueService();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<PurpleScreen>())
        {
            var isBarierClosed = other.gameObject.GetComponent<PurpleScreen>().IsScreenTurnOn();
            TurnPurpleScreens();            
            TurnPurpleGates(isBarierClosed);
            gateCloser.CloseTheGatesExceptPurple();
        }
    }    
    private void TurnPurpleScreens()
    {
        foreach (PurpleScreen item in listOfPurpleScreens)
        {
            item.TurnScreens();
        }        
    }
    private void TurnPurpleGates(bool isBarierClosed)
    {        
        foreach (PurpleBarier item in listOfPurpleBariers)
        {
            item.TurnBariers(isBarierClosed);
        }
    }
    private void IssueService()
    {
        if (listOfPurpleScreens == null) Debug.Log("CollisionWihPurpleScreen.Start. Can't find listOfPurpleScreens.");
        if (listOfPurpleBariers == null) Debug.Log("CollisionWihPurpleScreen.Start. Can't find listOfPurpleBariers.");
        if (gateCloser == null) Debug.Log("CollisionWihPurpleScreen.Start. Can't find gateCloser.");
        else return;
    }
}
