using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collisions : MonoBehaviour
{

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Door>())
        {
            PlayerCollidingWithDoor();
        }
        if (other.gameObject.GetComponent<Cherry>())
        {
            other.gameObject.SetActive(false);
            PlayerColidingWithCherry();
        }
        if (other.gameObject.GetComponent<Ghost>())
        {
            PlayerColidingWithGhost();
        }
        if (other.gameObject.GetComponent<InteractiveScreen>())
        {
            PlayerColidingWithScreen();
        }
    }
    private void PlayerCollidingWithDoor()
    {
        gameObject.GetComponent<WithDoorInteractor>().PlayerCollidingWithDoor();
    }
    private void PlayerColidingWithCherry()
    {
        gameObject.GetComponent<CherrysCollectingCounter>().CollectTheCherry();
    }
    private void PlayerColidingWithGhost()
    {
        gameObject.GetComponent<WithEnemyInteractor>().ColidingWithGhost();
    }
    private void PlayerColidingWithScreen()
    {
        gameObject.GetComponent<WithScreenInteractor>().InteractionWithScreen();
    }    
}
