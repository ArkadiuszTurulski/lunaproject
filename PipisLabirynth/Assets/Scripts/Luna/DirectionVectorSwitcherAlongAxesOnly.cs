using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirectionVectorSwitcherAlongAxesOnly : MonoBehaviour   //ogranicza kierunki wektora ruchu wy��cznie w pionie lub poziomie
{
    private NewJoystick joystick;
    private Vector2 directionVector;

    void Start()
    {
        joystick = FindObjectOfType<NewJoystick>();        
        IssueService();
    }

    void Update()
    {
        GetDirectionVectorFromJoystick();
        SwitchDirectionOfInputVectorOnlyAlongAxisXandY(directionVector);
    }

    public Vector2 GetDirectionVector()
    {
        return directionVector;
    }
    private void GetDirectionVectorFromJoystick()
    {
        directionVector = joystick.GetDirectionVector();
    }
    private void SwitchDirectionOfInputVectorOnlyAlongAxisXandY(Vector2 vec)
    {
        if (Mathf.Abs(directionVector.x) >= Mathf.Abs(directionVector.y))
        {
            directionVector.y = 0;
        }
        else { directionVector.x = 0; }
    }

    private void IssueService()
    {
        if (!joystick)
            Debug.Log("PlayerMover.Start.Can't find Joystick");       
    }
}
