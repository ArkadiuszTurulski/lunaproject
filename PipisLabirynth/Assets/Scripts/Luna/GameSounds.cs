using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSounds : MonoBehaviour
{
    private AudioSource audioSource;
    [SerializeField] AudioClip step1audioClip;
    [SerializeField] AudioClip step2audioClip;
    [SerializeField] AudioClip screensAudioClip;
    [SerializeField] AudioClip gatesAudioClip;
    [SerializeField] AudioClip lunaHappyClip;
    [SerializeField] AudioClip cherryCollectClip;
    [SerializeField] AudioClip lunaDieClip;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        IssueService();
    }

    public void LunaHappyClip() { AudioSource.PlayClipAtPoint(lunaHappyClip, this.transform.position); }
    public void ComputerSound() { AudioSource.PlayClipAtPoint(screensAudioClip, this.transform.position);  }
    public void BarierSound() { AudioSource.PlayClipAtPoint(gatesAudioClip, this.transform.position);  }
    public void CollectingCherrySound() { AudioSource.PlayClipAtPoint(cherryCollectClip, this.transform.position);  }
    public void LunaDyingSound() { AudioSource.PlayClipAtPoint(lunaDieClip, this.transform.position);  }


    public void SoundOfStep1()  { audioSource.PlayOneShot(step1audioClip);  }
    public void SoundOfStep2()  { audioSource.PlayOneShot(step2audioClip);  }
    

    private void IssueService()
    {
        if (!audioSource)
            Debug.Log("GameSounds.Start.Can't find audioSource");
        if (!step1audioClip)
            Debug.Log("GameSounds.Start.Can't find step1audioClip");
        if (!step2audioClip)
            Debug.Log("GameSounds.Start.Can't find step2audioClip");
        if (!screensAudioClip)
            Debug.Log("GameSounds.Start.Can't find screensAudioClip");
        if (!gatesAudioClip)
            Debug.Log("GameSounds.Start.Can't find gatesAudioClip");
        if (!lunaHappyClip)
            Debug.Log("GameSounds.Start.Can't find lunaHappyClip");
        if (!cherryCollectClip)
            Debug.Log("GameSounds.Start.Can't find cherryCollectClip");
        if (!lunaDieClip)
            Debug.Log("GameSounds.Start.Can't find lunaDieClip");
    }
}
