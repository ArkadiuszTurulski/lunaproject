using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GateCloser : MonoBehaviour //Obsluguje zamykanie otwartych bram z jednoczesnym wlaczaniem ekranow powiazanych z ww bramami
{    
    private List<IColourBarier> listOfTheBariers = new List<IColourBarier>();
    private List<IColorScreen> listOfTheScreens = new List<IColorScreen>();

    void Start()
    {
        listOfTheScreens = gameObject.GetComponent<ScreensInvertory>().GetListOfAllScreens();
        listOfTheBariers = gameObject.GetComponent<GatesInvertory>().GetListOfAllBarriers();
        IssueService();
    }    

    public void CloseTheGatesExceptBlue()
    {
        TurnOnScreensExceptBlueOne();
        CloseBarriersExceptBlueOne();
    }
    public void CloseTheGatesExceptGreen()
    {
        TurnOnScreensExceptGreenOne();
        CloseBarriersExceptGreenOne();
    }
    public void CloseTheGatesExceptPink()
    {
        TurnOnScreensExceptPinkOne();
        CloseBarriersExceptPinkOne();
    }
    public void CloseTheGatesExceptPurple()
    {
        TurnOnScreensExceptPurpleOne();
        CloseBarriersExceptPurpleOne();
    }

    private void TurnOnScreensExceptBlueOne()
    {        
        foreach (IColorScreen item in listOfTheScreens)
        {
            item.TurnTheScreensExcept("Blue");
        }
    }
    private void CloseBarriersExceptBlueOne()
    {        
        foreach (IColourBarier item in listOfTheBariers)
        {
            item.CloseBariers("Blue");
        }
    }
    private void TurnOnScreensExceptGreenOne()
    {        
        foreach (IColorScreen item in listOfTheScreens)
        {
            item.TurnTheScreensExcept("Green");
        }
    }
    private void CloseBarriersExceptGreenOne()
    {        
        foreach (IColourBarier item in listOfTheBariers)
        {
            item.CloseBariers("Green");
        }
    }
    private void TurnOnScreensExceptPinkOne()
    {        
        foreach (IColorScreen item in listOfTheScreens)
        {
            item.TurnTheScreensExcept("Pink");
        }
    }
    private void CloseBarriersExceptPinkOne()
    {        
        foreach (IColourBarier item in listOfTheBariers)
        {
            item.CloseBariers("Pink");
        }
    }
    private void TurnOnScreensExceptPurpleOne()
    {        
        foreach (IColorScreen item in listOfTheScreens)
        {
            item.TurnTheScreensExcept("Purple");
        }
    }
    private void CloseBarriersExceptPurpleOne()
    {        
        foreach (IColourBarier item in listOfTheBariers)
        {
            item.CloseBariers("Purple");
        }
    }
    private void IssueService()
    {
        if (listOfTheScreens == null) Debug.Log("GateCloser.Start. Can't find listOfTheScreens.");
        if (listOfTheBariers == null) Debug.Log("GateCloser.Start. Can't find listOfTheBariers.");
    }
}
