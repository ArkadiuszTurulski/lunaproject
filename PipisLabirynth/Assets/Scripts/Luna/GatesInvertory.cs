using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GatesInvertory : MonoBehaviour //Tworzy liste wszystkich bram obecnych na aktualnym poziomie
{
    private BlueBarier[] listOfBlueBariers;
    private GreenBarier[] listOfGreenBariers;
    private PinkBarier[] listOfPinkBariers;
    private PurpleBarier[] listOfPurpleBariers;

    private List<IColourBarier> listOfTheBariers = new List<IColourBarier>();

    void Start()
    {
        FindTheBarriersOfAllColors();
        AddAllToListOfTheBarriers();
    }
    public List<IColourBarier> GetListOfAllBarriers()
    {
        return listOfTheBariers;
    }
    private void FindTheBarriersOfAllColors()
    {
        listOfBlueBariers = FindObjectsOfType<BlueBarier>();
        listOfGreenBariers = FindObjectsOfType<GreenBarier>();
        listOfPinkBariers = FindObjectsOfType<PinkBarier>();
        listOfPurpleBariers = FindObjectsOfType<PurpleBarier>();
    }
    private void AddAllToListOfTheBarriers()
    {
        foreach (BlueBarier item in listOfBlueBariers)
        {
            listOfTheBariers.Add(item);
        }
        foreach (GreenBarier item in listOfGreenBariers)
        {
            listOfTheBariers.Add(item);
        }
        foreach (PinkBarier item in listOfPinkBariers)
        {
            listOfTheBariers.Add(item);
        }
        foreach (PurpleBarier item in listOfPurpleBariers)
        {
            listOfTheBariers.Add(item);
        }
    }
}
