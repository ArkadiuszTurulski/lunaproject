using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LunaChildrenInteractionsController : MonoBehaviour
{
    //Klasa ta znajduje si� w ka�dym z children, podobnie jak animatorzy. AudioSource jest osadzony w parent.
    //Klasa ma za zadanie synchronizowac animacje i d�wi�ki. 
    private Animator animator;
    private GameSounds gameSounds;
    private NewJoystick joystick;

    void Start()
    {
        animator = gameObject.GetComponent<Animator>();
        gameSounds = GetComponentInParent<GameSounds>();
        joystick = FindObjectOfType<NewJoystick>();
        IssueService();
    }

    public void Restart() { FindObjectOfType<Door>().Restart(); }
    public void LoadNextLevel() { FindObjectOfType<Door>().LoadNextLevel(); }
    public void StartJoystick() { joystick.StartJoystick(); }

    public void TriggerAnimation(string trigger) { animator.SetTrigger(trigger); }
        
    public void ComputerSound() { gameSounds.ComputerSound(); } 
    public void BarierSound() { gameSounds.BarierSound(); } 

    public void SoundOfStep1() { gameSounds.SoundOfStep1(); }
    public void SoundOfStep2() { gameSounds.SoundOfStep2(); }

    private void IssueService()
    {
        if (!animator)
            Debug.Log("LunaChildrenInteractionsController.Start.Can't find animator");
        if (!gameSounds)
            Debug.Log("LunaChildrenInteractionsController.Start.Can't find gameSounds");        
        if (!joystick)
            Debug.Log("LunaChildrenInteractionsController.Start.Can't find joystick");
    }
}
