using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LunaController : MonoBehaviour
{
    //Obiekt playera to w rzeczywisto�ci trzy r�ne obiekty posiadaj�ce odmienne animacje
    //S� one obrazem playera widzianym z boku, ty�u i przodu
    //Wszystkie s� dzieckiem jednego rodzica, w kt�rym zawarty jest ten skrypt
    //To, kt�ry jest aktualnie aktywny zale�y od ruchu joysticka, czyli kierunku, w ktorym przemieszcza sie player
    //Je�li joystick kierowany jest w poziomie, player zmienia skal� mi�dzy -1 a 1, zale�nie od kierunku marszu

    [SerializeField] GameObject lunaSideChild;
    [SerializeField] GameObject lunaBackChild;
    [SerializeField] GameObject lunaFrontChild;
    private Vector2 directionVector;   


    void Start()
    {
        SetActiveHorizontalPlayer();
        IssueService();
    }

    void Update()
    {
        directionVector = GetComponent<DirectionVectorSwitcherAlongAxesOnly>().GetDirectionVector();
        if (isPlayerMovingHorizontalDirection()) 
        {
            SetActiveHorizontalPlayer();            
            FlipPlayerWhenIsChangingDirection();
        }
        else if (isPlayerMovingUpDirection())
        {
            SetActiveBackPlayer();            
        }
        else if (isPlayerMovingDownDirection())
        {
            SetActiveFrontplayer();            
        }
    }
    private bool isPlayerMovingHorizontalDirection()
    {
        if (directionVector.y == 0 && directionVector.x != 0) return true;
        else return false;
    }
    private bool isPlayerMovingUpDirection()
    {
        if (directionVector.y > 0) return true;
        else return false;
    }
    private bool isPlayerMovingDownDirection()
    {
        if (directionVector.y < 0) return true;
        else return false;
    }

    private void SetActiveHorizontalPlayer()
    {
        lunaSideChild.SetActive(true);
        lunaBackChild.SetActive(false);
        lunaFrontChild.SetActive(false);
    }
    private void SetActiveBackPlayer()
    {
        lunaSideChild.SetActive(false);
        lunaBackChild.SetActive(true);
        lunaFrontChild.SetActive(false);
    }
    private void SetActiveFrontplayer()
    {
        lunaSideChild.SetActive(false);
        lunaBackChild.SetActive(false);
        lunaFrontChild.SetActive(true);
    }
    private void FlipPlayerWhenIsChangingDirection()
    {
        Vector2 scaleX = lunaSideChild.transform.localScale;
        if (directionVector.x > 0)  scaleX.x = -1;            
        else if (directionVector.x < 0)  scaleX.x = 1;
        else  return; 
        lunaSideChild.transform.localScale = scaleX;        
    }

    private void IssueService()
    {
        if (!lunaSideChild)
            Debug.Log("LunaController.Start.Can't find lunaSideChild");
        if (!lunaBackChild)
            Debug.Log("LunaController.Start.Can't find lunaBackChild");
        if (!lunaFrontChild)
            Debug.Log("LunaController.Start.Can't find lunaFrontChild");        
    }


}
