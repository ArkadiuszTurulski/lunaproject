using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationRunner : MonoBehaviour  //zmienia animacje miedzy "iddle" a "running" zaleznie od tego czy player stoi, czy porusza sie
{

    private VelocityGetter velocityGetter;
    private Vector3 playerVelocity;
    private bool isPlayerRunning = false;
    private Animator animator;

    void Start()
    {
        animator = gameObject.GetComponent<Animator>();
        velocityGetter = FindObjectOfType<VelocityGetter>();
        IssueService();
    }
    void Update()
    {
        TriggerRunningAnimationIfVelocityExists();
    }
    private void TriggerRunningAnimationIfVelocityExists()
    {
        isPlayerRunning = isVelocityExists();
        animator.SetBool("Run", isPlayerRunning);
    }
    private bool isVelocityExists()
    {
        playerVelocity = velocityGetter.GetPlayerVelocity();
        if (playerVelocity != Vector3.zero)   return true;
        else  return false; 
    }

    private void IssueService()
    {
        if (!animator)
            Debug.Log("PlayerAnimationRunner.Start.Can't find animator");
        if (!velocityGetter)
            Debug.Log("PlayerAnimationRunner.Start.Can't find velocityGetter");
    }
}
