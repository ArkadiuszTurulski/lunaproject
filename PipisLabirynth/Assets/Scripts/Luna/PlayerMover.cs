using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMover : MonoBehaviour    //porusza playera zgodnie z wektorem ruchu
{
    [SerializeField] CharacterController characterController;
    [SerializeField] float speed = 1;
    private DirectionVectorSwitcherAlongAxesOnly directionVectorSwitcher;
    private Vector2 directionVector;
    

    void Start()
    {
        directionVectorSwitcher = gameObject.GetComponent<DirectionVectorSwitcherAlongAxesOnly>();
        IssueService();
    }

    void Update()
    {
        GetDirectionVectorFromDirectionVectorSwitcherAlongAxesOnly();
        MoveThePlayerObject();
    }
    
    private void GetDirectionVectorFromDirectionVectorSwitcherAlongAxesOnly()
    {
        directionVector = directionVectorSwitcher.GetDirectionVector();
    }
    private void MoveThePlayerObject()
    {
        characterController.Move(directionVector * Time.deltaTime * speed);
    }


    private void IssueService()
    {        
        if (!characterController)
            Debug.Log("PlayerMover.Start.Can't find CharacterController");
        if (speed == 0f)
            Debug.Log("PlayerMover.Start.Set speed!");
        if (!directionVectorSwitcher)
            Debug.Log("PlayerMover.Start.Can't find directionVectorSwitcher");
    }
}
