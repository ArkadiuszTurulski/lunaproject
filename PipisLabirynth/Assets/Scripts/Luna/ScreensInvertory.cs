using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreensInvertory : MonoBehaviour   //Tworzy liste wszystkich ekranow obecnych na aktualnym poziomie
{
    private BlueScreen[] listOfBlueScreens;
    private GreenScreen[] listOfGreenScreens;
    private PinkScreen[] listOfPinkScreens;
    private PurpleScreen[] listOfPurpleScreens;

    private List<IColorScreen> listOfTheScreens = new List<IColorScreen>();

    void Start()
    {
        FindTheScreensOfAllColors();
        AddAllToListOfTheScreens();
    }
    public List<IColorScreen> GetListOfAllScreens()
    {
        return listOfTheScreens;
    }
    private void FindTheScreensOfAllColors()
    {
        listOfBlueScreens = FindObjectsOfType<BlueScreen>();
        listOfGreenScreens = FindObjectsOfType<GreenScreen>();
        listOfPinkScreens = FindObjectsOfType<PinkScreen>();
        listOfPurpleScreens = FindObjectsOfType<PurpleScreen>();
    }
    private void AddAllToListOfTheScreens()
    {
        foreach (BlueScreen item in listOfBlueScreens)
        {
            listOfTheScreens.Add(item);
        }
        foreach (GreenScreen item in listOfGreenScreens)
        {
            listOfTheScreens.Add(item);
        }
        foreach (PinkScreen item in listOfPinkScreens)
        {
            listOfTheScreens.Add(item);
        }
        foreach (PurpleScreen item in listOfPurpleScreens)
        {
            listOfTheScreens.Add(item);
        }
    }
}
