using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VelocityGetter : MonoBehaviour
{
    [SerializeField] CharacterController characterController;
    private Vector3 playerVelocity;

    public Vector3 GetPlayerVelocity()
    {
        playerVelocity = characterController.velocity;
        return playerVelocity;
    }
}
