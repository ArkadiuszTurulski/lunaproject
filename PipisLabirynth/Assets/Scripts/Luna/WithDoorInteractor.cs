using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WithDoorInteractor : MonoBehaviour
{
    CherryInfo cherryInfoText;

    void Start()
    {
        cherryInfoText = FindObjectOfType<CherryInfo>();
    }

    public void PlayerCollidingWithDoor()
    {
        
        if (isEveryCherryCollected())
        {
            ActivateLunnaHappyAudioClip();
            StopJoystick();
            TriggerAnimationBeHappy();
        }
        else
        {
            ActivateCherryCollectingRequestInfo();
            return;
        }
    }
    private bool isEveryCherryCollected()
    {
        int cherrysLeftNumber = FindObjectsOfType<Cherry>().Length;
        if (cherrysLeftNumber == 0) return true;
        else return false;
    }
    private void ActivateCherryCollectingRequestInfo()
    {
        cherryInfoText.gameObject.SetActive(true);
    }
    private void ActivateLunnaHappyAudioClip()
    {
        FindObjectOfType<GameSounds>().LunaHappyClip();
    }
    private void StopJoystick()
    {
        FindObjectOfType<NewJoystick>().StopJoystick();
    }
    private void TriggerAnimationBeHappy()
    {
        FindObjectOfType<LunaChildrenInteractionsController>().TriggerAnimation("Happy"); 
    }
}
