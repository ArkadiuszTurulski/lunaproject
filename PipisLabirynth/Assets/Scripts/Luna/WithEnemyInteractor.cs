using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WithEnemyInteractor : MonoBehaviour
{

    public void ColidingWithGhost()
    {
        ActivateFailureAudioClip();
        StopJoystick();
        TriggerAnimationDie();
    }
    private void ActivateFailureAudioClip()
    {
        FindObjectOfType<GameSounds>().LunaDyingSound();
    }
    private void StopJoystick()
    {
        FindObjectOfType<NewJoystick>().StopJoystick();
    }
    private void TriggerAnimationDie()
    {        
        FindObjectOfType<LunaChildrenInteractionsController>().TriggerAnimation("Die");
    }
}
