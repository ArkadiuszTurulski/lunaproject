using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WithScreenInteractor : MonoBehaviour
{

    public void InteractionWithScreen()
    {
        StopJoystick();
        TriggerAnimationInterAct();
    }
    private void StopJoystick()
    {
        FindObjectOfType<NewJoystick>().StopJoystick();
    }
    private void TriggerAnimationInterAct()
    {
        FindObjectOfType<LunaChildrenInteractionsController>().TriggerAnimation("Interact");
    }
}
