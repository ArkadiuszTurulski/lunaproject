using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlueScreen : InteractiveScreen, IColorScreen
{
    private string color = "Blue";

    public void TurnTheScreensExcept(string color)
    {
        if (this.color != color)
            base.TurnOnScreens();
        else return;
    }
}
