using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GreenScreen : InteractiveScreen, IColorScreen
{
    private string color = "Green";

    public void TurnTheScreensExcept(string color)
    {
        if (this.color != color)
            base.TurnOnScreens();
        else return;
    }
}
