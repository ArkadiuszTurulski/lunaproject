using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IColorScreen 
{
    public void TurnTheScreensExcept(string color);
}
