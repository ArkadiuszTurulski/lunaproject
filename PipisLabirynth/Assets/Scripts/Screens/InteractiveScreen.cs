using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveScreen : MonoBehaviour  //klasa bazowa dla klas identyfikujacych ekrany konkretnego koloru
{
    private Animator anime;
    private bool isScreenTurnOn = true;

    void Start()
    {
        anime = gameObject.GetComponent<Animator>();
    }
    public bool IsScreenTurnOn()
    {
        return isScreenTurnOn;
    }
    public void TurnScreens()
    {
        if (isScreenTurnOn)
        {
            anime.SetTrigger("ScreenOff");
            isScreenTurnOn = false;
        }
        else
        {
            anime.SetTrigger("ScreenOn");
            isScreenTurnOn = true;
        }
    }
    public void TurnOnScreens()
    {
        if (!isScreenTurnOn)
        {
            anime.SetTrigger("ScreenOn");
            isScreenTurnOn = true;
        }
        else { return; }
    }        
}
