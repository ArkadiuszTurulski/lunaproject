using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinkScreen : InteractiveScreen, IColorScreen
{
    private string color = "Pink";

    public void TurnTheScreensExcept(string color)
    {
        if (this.color != color)
            base.TurnOnScreens();
        else return;
    }
}
