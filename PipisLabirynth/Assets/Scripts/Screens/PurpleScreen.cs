using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PurpleScreen : InteractiveScreen, IColorScreen
{
    private string color = "Purple";

    public void TurnTheScreensExcept(string color)
    {
        if (this.color != color)
            base.TurnOnScreens();
        else return;
    }
}
