using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnapToGrid : MonoBehaviour
{
    private void OnDrawGizmos()
    {
        SnapToTheGrid();
    }
    private void SnapToTheGrid()
    {
        
        var position = new Vector3(Mathf.RoundToInt(this.transform.position.x), Mathf.RoundToInt(this.transform.position.y), 0f);
        this.transform.position = position;
    }
}
